class AppError extends Error {
  //inheriting from the parent class Error
  constructor(message, statusCode) {
    //object will take the message and the statuscode
    //call parent constructor using super
    super(message);

    this.statusCode = statusCode;
    this.status = `${statusCode}`.startsWith("4") ? "fail" : "error";
    this.isOperational = true;

    Error.captureStackTrace(this, this.constructor);
  }
}

module.exports = AppError;
